# To execute command...bonus("car trips.csv",'GO',1)
# Bonus....5%,10%,15%
# slabs....1 to 6 trips, 7 to 12 trips  & more than 12 trips
bonus<-function(filename,cartype,Days=1:28) {
data<-na.omit(read.csv(filename), header=TRUE)
Go.Bonus<-numeric()
Accu.GO.Bonus<-numeric()
Earn.Numeric<-numeric()
Goplus.Bonus<-numeric()
Accu.GOplus.Bonus<-numeric()
Business.Bonus<-numeric()
Accu.Business.Bonus<-numeric()
Trips<-numeric()
Earnings<-numeric()
if (cartype=="GO") {
for (i in Days) {
# Give bonus to GO type cars according to no of trips completed
if (data$GO[i]>=1 & data$GO[i]<= 6){

Earn.Numeric<-as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]])

Trips<-c(Trips,data$GO[i])
Earnings<-c(Earnings,as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]]))

Go.Bonus<-c(Go.Bonus,Earn.Numeric*0.05)
Accu.GO.Bonus<-c(Accu.GO.Bonus,sum(Go.Bonus))
}else if (data$GO[i]>=7 & data$GO[i]<=12){

Earn.Numeric<-as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]])

Trips<-c(Trips,data$GO[i])
Earnings<-c(Earnings,as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]]))

Go.Bonus<-c(Go.Bonus,Earn.Numeric*0.10)
Accu.GO.Bonus<-c(Accu.GO.Bonus,sum(Go.Bonus))
}else if (data$GO[i]>=13){

Earn.Numeric<-as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]])

Trips<-c(Trips,data$GO[i])
Earnings<-c(Earnings,as.numeric(levels(data$GO_Earn[i])[data$GO_Earn[i]]))

Go.Bonus<-c(Go.Bonus,Earn.Numeric*0.15)
Accu.GO.Bonus<-c(Accu.GO.Bonus,sum(Go.Bonus))
}
}
data.frame(Days,Trips,Earnings,Go.Bonus,Accu.GO.Bonus)

# Give bonus to GOplus type cars according to no of trips completed
} else if (cartype=="GOplus" ) {

for (i in Days) {
  
if (data$GOplus[i]>=1 & data$GOplus[i]<= 6){
  
Earn.Numeric<-as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]])

Trips<-c(Trips,data$GOplus[i])
Earnings<-c(Earnings,as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]]))

Goplus.Bonus<-c(Goplus.Bonus,Earn.Numeric*0.05)
Accu.GOplus.Bonus<-c(Accu.GOplus.Bonus,sum(Goplus.Bonus))
}else if (data$GOplus[i]>=7 & data$GOplus[i]<=12){
  
Earn.Numeric<-as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]])
Trips<-c(Trips,data$GOplus[i])
Earnings<-c(Earnings,as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]]))

Goplus.Bonus<-c(Goplus.Bonus,Earn.Numeric*0.10)
Accu.GOplus.Bonus<-c(Accu.GOplus.Bonus,sum(Goplus.Bonus))
}else if (data$GOplus[i]>=13){
  
Earn.Numeric<-as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]])
Trips<-c(Trips,data$GOplus[i])
Earnings<-c(Earnings,as.numeric(levels(data$GOplus_Earn[i])[data$GOplus_Earn[i]]))

Goplus.Bonus<-c(Goplus.Bonus,Earn.Numeric*0.15)
Accu.GOplus.Bonus<-c(Accu.GOplus.Bonus,sum(Goplus.Bonus))
}
  
# Give bonus to Business type cars according to no of trips completed
}
data.frame(Days,Trips,Earnings,Goplus.Bonus,Accu.GOplus.Bonus)
} else if (cartype=="Business" ) {
for (i in Days) {

if (data$Business[i]>=1 & data$Business[i]<= 6){

Earn.Numeric<-as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]])

Trips<-c(Trips,data$Business[i])
Earnings<-c(Earnings,as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]]))

Business.Bonus<-c(Business.Bonus,Earn.Numeric*0.05)
Accu.Business.Bonus<-c(Accu.Business.Bonus,sum(Business.Bonus))
}else if (data$Business[i]>=7 & data$Business[i]<=12){
  
Earn.Numeric<-as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]])

Trips<-c(Trips,data$Business[i])
Earnings<-c(Earnings,as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]]))

Business.Bonus<-c(Business.Bonus,Earn.Numeric*0.10)
Accu.Business.Bonus<-c(Accu.Business.Bonus,sum(Business.Bonus))
}else if (data$Business[i]>=13){
  
Earn.Numeric<-as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]])

Trips<-c(Trips,data$Business[i])
Earnings<-c(Earnings,as.numeric(levels(data$Business_Earn[i])[data$Business_Earn[i]]))

Business.Bonus<-c(Business.Bonus,Earn.Numeric*0.15)
Accu.Business.Bonus<-c(Accu.Business.Bonus,sum(Business.Bonus))
}
  
}
data.frame(Days,Trips,Earnings,Business.Bonus,Accu.Business.Bonus)
}else { print("check function arguments") }

}
